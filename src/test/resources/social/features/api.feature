@api
Feature: API test

  Scenario: Test if API is running
    Given I want to get status of the API
    Then response should contain message "Application up and Running!"
    And response http status should be 200
    And traceId should be present