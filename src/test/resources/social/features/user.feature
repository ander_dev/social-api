@user
Feature: Users tests

  Background: Load userDTO from minimum
    Given a new "user" is loaded using "user-minimum"

  Scenario: New user with email address already used
    Given user loaded is submitted
    And response should contain message "User successfully saved!"
    When a new "user" is loaded using "user-minimum"
    And user loaded is submitted
    Then response should contain message "Email already in use!"
    And response http status should be 400
    And traceId should be present

  Scenario: New user without inform password
    Given user loaded does not have password
    When user loaded is submitted
    Then response should contain message "Password must be informed!"
    And response http status should be 400
    And traceId should be present

  Scenario: New user without inform name
    Given user loaded does not have name
    When user loaded is submitted
    Then response should contain message "Name must be informed!"
    And response http status should be 400
    And traceId should be present

  Scenario: New user without inform email
    Given user loaded does not have email
    When user loaded is submitted
    Then response should contain message "E-mail address must be informed!"
    And response http status should be 400
    And traceId should be present

  Scenario: New user successfully created
    Given user loaded is submitted
    And response should contain message "User successfully saved!"
    And response http status should be 200
    And traceId should be present

  Scenario: Editing user
    Given user loaded is submitted
    And I get user just created
    When user name is changed to "New user automation"
    And user loaded is submitted
    Then response should contain message "User successfully updated!"
    And response http status should be 200
    And traceId should be present
    And user name is equals to "New user automation"

  Scenario: Deleting user
    Given user loaded is submitted
    When I want to delete user just created
    Then response should contain message "User successfully deleted!"
    And response http status should be 200
    And traceId should be present
    And user excluded is equals to "true"

  Scenario: Login successfully
    Given user loaded is submitted
    And response should contain message "User successfully saved!"
    And response http status should be 200
    And traceId should be present
    When login is requested with email "automation.user.do.not.use@gmail.com" and password "123456"
    Then response should contain message "Automation User successfully logged in!"

  Scenario: Login error
    Given user loaded is submitted
    And response should contain message "User successfully saved!"
    And response http status should be 200
    And traceId should be present
    When login is requested with email "automation.user.do.not.use@gmail.com" and password "1234567"
    Then response should contain message "Please verify your credentials!"