@comment
Feature: Comments tests

  Background: Create user and post for the user
    Given a new "user" is loaded using "user-minimum"
    And user loaded is submitted
    And a new "post" is loaded using "post-minimum"
    And post loaded contains user just created
    And post loaded is submitted

  Scenario: New comment without User
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains post just created
    When comment loaded is submitted
    Then response should contain message "User must be informed!"
    And response http status should be 400
    And traceId should be present

  Scenario: New comment without Post
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    When comment loaded is submitted
    Then response should contain message "Post must be informed!"
    And response http status should be 400
    And traceId should be present

  Scenario: New comment without message
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded does not have message
    And comment loaded contains user just created
    And comment loaded contains post just created
    When comment loaded is submitted
    Then response should contain message "Comment message must be informed!"
    And response http status should be 400
    And traceId should be present

  Scenario: New comment successfully created
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    And comment loaded contains post just created
    When comment loaded is submitted
    Then response should contain message "Comment successfully saved!"
    And response http status should be 200
    And traceId should be present

  Scenario: Comments successfully listed by post
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    And comment loaded contains post just created
    And comment loaded is submitted
    And response should contain message "Comment successfully saved!"
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    And comment loaded contains post just created
    And comment loaded is submitted
    And response should contain message "Comment successfully saved!"
    When the list of comments for current post is requested
    Then response should contain message "Comments successfully listed!"
    And response http status should be 200
    And response should contain 2 objects
    And traceId should be present

  Scenario: Comments successfully listed by user
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    And comment loaded contains post just created
    And comment loaded is submitted
    And response should contain message "Comment successfully saved!"
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    And comment loaded contains post just created
    And comment loaded is submitted
    And a new "post" is loaded using "post-minimum"
    And post loaded contains user just created
    And post loaded is submitted
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    And comment loaded contains post just created
    And comment loaded is submitted
    And response should contain message "Comment successfully saved!"
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    And comment loaded contains post just created
    And comment loaded is submitted
    And response should contain message "Comment successfully saved!"
    When the list of comments for current user is requested
    Then response should contain message "Comments successfully listed!"
    And response http status should be 200
    And response should contain 4 objects
    And traceId should be present

  Scenario: New comment successfully edited
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    And comment loaded contains post just created
    When comment loaded is submitted
    Then response should contain message "Comment successfully saved!"
    Given I get comment just created
    And I change comment message to "New comment from automation"
    When comment loaded is submitted
    Then response should contain message "Comment successfully updated!"
    And comment message is changed to "New comment from automation"

  Scenario: Delete comment
    Given a new "comment" is loaded using "comment-minimum"
    And comment loaded contains user just created
    And comment loaded contains post just created
    When comment loaded is submitted
    Then response should contain message "Comment successfully saved!"
    When I request to delete a comment just created
    Then response should contain message "Comment successfully deleted!"
    And response http status should be 200