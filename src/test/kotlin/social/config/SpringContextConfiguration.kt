package social.config

import io.cucumber.java8.En
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootContextLoader
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.test.context.ContextConfiguration
import social.Application
import social.service.UserService

@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
@ContextConfiguration(classes = [Application::class], loader = SpringBootContextLoader::class)
class SpringContextConfiguration: En {

    private val logger = KotlinLogging.logger {}

    @Autowired
    lateinit var userService: UserService

    init {
        Before{ _ ->
            logger.info("-------------- Spring Context Initialized For Executing Cucumber Tests --------------")
        }

        After{ _ ->
            logger.info("-------------- Spring Context clean DB for next text with Cucumber Tests --------------")
            userService.deleteUserAutomation("automation.user.do.not.use@gmail.com")
        }
    }
}