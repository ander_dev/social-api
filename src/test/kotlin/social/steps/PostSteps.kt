package social.steps

import io.cucumber.java8.En
import io.cucumber.java8.Scenario
import org.junit.Assert
import social.services.PostStepsService

var postStepsLastInstance: PostSteps? = null


class PostSteps : En, PostStepsService() {

    init {

        Before { _: Scenario ->
            Assert.assertNotSame(this, postStepsLastInstance)
            postStepsLastInstance = this
        }

        After { _: Scenario ->
            Assert.assertSame(this, postStepsLastInstance)
            postStepsLastInstance = this
        }

        Given("post loaded does not have message") {
            postDTO.message = null
        }

        Given("post loaded contains user just created") {
            postDTO.user = userDTO
        }

        Given("post loaded is submitted") {
            post()
        }

        Given("the list of post for current user is request") {
            list(userDTO.id!!)
        }

        Given("I get post just created") {
            get(postDTO.id!!)
        }

        And("I change post message to {string}") { message: String ->
            postDTO.message = message
        }

        And("post message is changed to {string}") { message: String ->
            Assert.assertEquals(postDTO.message, message)
        }

        When("I request to delete a Post just created") {
            delete(postDTO.id!!, postDTO.user!!.id!!)
        }
    }
}