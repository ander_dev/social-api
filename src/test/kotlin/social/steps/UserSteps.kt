package social.steps

import io.cucumber.java8.En
import io.cucumber.java8.Scenario
import org.junit.Assert
import social.services.UserStepsService

var userStepsLastInstance: UserSteps? = null


class UserSteps : En, UserStepsService() {

    init {

        Before { _: Scenario ->
            Assert.assertNotSame(this, userStepsLastInstance)
            userStepsLastInstance = this
        }

        After { _: Scenario ->
            Assert.assertSame(this, userStepsLastInstance)
            userStepsLastInstance = this
        }

        Given("user loaded does not have email") {
            userDTO.email = null
        }

        Given("user loaded does not have password") {
            userDTO.password = null
        }

        Given("user loaded does not have name") {
            userDTO.name = null
        }

        Given("user loaded is submitted") {
            post()
        }

        Given("I get user just created") {
            get(userDTO.id!!)
        }

        When("user name is changed to {string}") {value: String ->
            userDTO.name = value
        }

        And ("user name is equals to {string}") { value: String ->
            Assert.assertEquals(value, userDTO.name)
        }

        When ("I want to delete user just created") {
            delete(userDTO.id!!)
        }

        And ("user excluded is equals to {string}") { value: String ->
            Assert.assertEquals(value.toBoolean(), userDTO.excluded)
        }

        When("login is requested with email {string} and password {string}") { email: String, password: String ->
            login(email, password)
        }
    }
}