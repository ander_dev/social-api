package social.steps

import io.cucumber.java8.En
import io.cucumber.java8.Scenario
import org.junit.Assert
import social.services.CommentStepsService

var commentStepsLastInstance: CommentSteps? = null


class CommentSteps : En, CommentStepsService() {

    init {

        Before { _: Scenario ->
            Assert.assertNotSame(this, commentStepsLastInstance)
            commentStepsLastInstance = this
        }

        After { _: Scenario ->
            Assert.assertSame(this, commentStepsLastInstance)
            commentStepsLastInstance = this
        }

        Given("comment loaded does not have message") {
            commentDTO.message = null
        }

        Given("comment loaded dos not have user") {
            commentDTO.user = null
        }

        Given("comment loaded contains user just created") {
            commentDTO.user = userDTO
        }

        Given("comment loaded contains post just created") {
            commentDTO.post = postDTO
        }

        Given("comment loaded is submitted") {
            post()
        }

        Given("the list of comments for current post is requested") {
            listByPostAndUser(postDTO.id!!, userDTO.id!!)
        }

        Given("the list of comments for current user is requested") {
            listByUser(userDTO.id!!)
        }

        Given("I get comment just created") {
            get(commentDTO.id!!)
        }

        And("I change comment message to {string}") { message: String ->
            commentDTO.message = message
        }

        And("comment message is changed to {string}") { message: String ->
            Assert.assertEquals(commentDTO.message, message)
        }

        When("I request to delete a comment just created") {
            delete(commentDTO.id!!, commentDTO.user!!.id!!)
        }
    }
}