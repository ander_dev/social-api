package social.services

import social.entity.dto.PostDTO
import social.entity.dto.ResponseDTO
import java.util.*

open class PostStepsService : RestService() {

    fun get(id: UUID) {
        responseDTO = get("$BASE_URL/$id")
        setDTO(responseDTO)
    }

    fun post() {
        responseDTO = post(BASE_URL, postDTO)
        setDTO(responseDTO)
    }

    fun list(id: UUID): ResponseDTO {
        return list("$BASE_URL/by-user/$id")
    }

    fun delete(postId: UUID, userId: UUID): ResponseDTO {
        return delete("$BASE_URL/$postId/$userId")
    }

    private fun setDTO(responseDTO: ResponseDTO){
        if(!responseDTO.objects.isNullOrEmpty()){
            postDTO = mapper.convertValue(responseDTO.objects?.get(0), PostDTO::class.java)
        }
    }

    companion object {
        const val BASE_URL = "/api/post"
    }
}