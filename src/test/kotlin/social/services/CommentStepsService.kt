package social.services

import social.entity.dto.CommentDTO
import social.entity.dto.ResponseDTO
import java.util.*

open class CommentStepsService : RestService() {

    fun get(id: UUID) {
        responseDTO = get("$BASE_URL/$id")
        setDTO(responseDTO)
    }

    fun post() {
        responseDTO = post(BASE_URL, commentDTO)
        setDTO(responseDTO)
    }

    fun listByPostAndUser(postId: UUID, userId: UUID): ResponseDTO {
        return list("$BASE_URL/by-post/$postId/$userId")
    }

    fun listByUser(userId: UUID): ResponseDTO {
        return list("$BASE_URL/by-user/$userId")
    }

    fun delete(postId: UUID, userId: UUID): ResponseDTO {
        return delete("$BASE_URL/$postId/$userId")
    }

    private fun setDTO(responseDTO: ResponseDTO){
        if(!responseDTO.objects.isNullOrEmpty()){
            commentDTO = mapper.convertValue(responseDTO.objects?.get(0), CommentDTO::class.java)
        }
    }

    companion object {
        const val BASE_URL = "/api/comment"
    }
}