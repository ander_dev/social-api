package social.services

import social.entity.dto.ResponseDTO
import social.entity.dto.UserDTO
import java.util.*

open class UserStepsService : RestService() {

    fun get(id: UUID) {
        responseDTO = get("$BASE_URL/$id")
        setDTO(responseDTO)
    }

    fun post() {
        responseDTO = post("$BASE_URL", userDTO)
        setDTO(responseDTO)
    }

    fun delete(id: UUID) {
        responseDTO =  delete("$BASE_URL/$id")
        setDTO(responseDTO)
    }

    fun login(email: String, password: String){
        login(email, password, "$BASE_URL/login")
    }

    private fun setDTO(responseDTO: ResponseDTO){
        if(!responseDTO.objects.isNullOrEmpty()) {
            userDTO = mapper.convertValue(responseDTO.objects?.get(0), UserDTO::class.java)
        }
    }

    companion object {
        const val BASE_URL = "/api/user"
    }
}