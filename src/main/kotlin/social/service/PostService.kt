package social.service

import social.entity.dto.PostDTO
import social.entity.dto.ResponseDTO
import java.util.*

interface PostService {

    fun save(postDTO: PostDTO) : ResponseDTO

    fun findAllByUser(userId: UUID) : ResponseDTO

    fun findById(id: UUID) : ResponseDTO

    fun findAll() : ResponseDTO

    fun delete(postId: UUID, userId: UUID): ResponseDTO

}

