package social.service

import social.entity.dto.CommentDTO
import social.entity.dto.ResponseDTO
import java.util.*

interface CommentService {

    fun save(commentDTO: CommentDTO) : ResponseDTO

    fun findAllByUser(userId: UUID) : ResponseDTO

    fun findAllByPostAndUser(postId: UUID, userId: UUID) : ResponseDTO

    fun findById(id: UUID) : ResponseDTO

    fun delete(commentId: UUID, userId: UUID): ResponseDTO

}

