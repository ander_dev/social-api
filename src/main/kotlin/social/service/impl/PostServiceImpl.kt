package social.service.impl

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import social.entity.Post
import social.entity.User
import social.entity.dto.PostDTO
import social.entity.dto.ResponseDTO
import social.exception.CustomException
import social.repository.PostRepository
import social.repository.UserRepository
import social.service.PostService
import java.util.*

@Service("postService")
class PostServiceImpl : PostService {

    private val logger = KotlinLogging.logger {}

    @Autowired
    lateinit var postRepository: PostRepository

    @Autowired
    lateinit var useRepository: UserRepository

    private fun validatePost(postDTO: PostDTO): MutableList<String>? {
        val errors: MutableList<String>? = ArrayList()
        if (postDTO.message.isNullOrEmpty()) {
            errors!!.add("Post message must be informed!")
        }
        if (postDTO.user == null || postDTO.user!!.id == null) {
            errors!!.add("User must be informed!")
        }
        return errors
    }

    @Transactional
    override fun save(postDTO: PostDTO): ResponseDTO {

        val post: Post
        val message = if (postDTO.id == null) "Post successfully saved!" else "Post successfully updated!"
        try {
            val errors = validatePost(postDTO)
            if (!errors.isNullOrEmpty()) return ResponseDTO.error(400, errors)

            val userResult: Optional<User> = useRepository.findById(postDTO.user!!.id!!)
            if (!userResult.isPresent) return ResponseDTO.error(400, mutableListOf("User does not exists!"))

            postDTO.user = userResult.get().toDTO()
            post = postRepository.save(Post.fromDTO(postDTO))
        } catch (e: Exception) {
            logger.error { e }
            throw CustomException(e.message!!)
        }
        return ResponseDTO.success(200, mutableListOf(post.toDTO()), mutableListOf(message))
    }

    override fun findAllByUser(userId: UUID): ResponseDTO {
        val userResult: Optional<User> = useRepository.findById(userId)
        if (!userResult.isPresent) return ResponseDTO.error(400, mutableListOf("User does not exists!"))

        val posts = postRepository.findAllByUserIdOrderByCreatedDesc(userId)
        val list: MutableList<Any> = ArrayList()
        posts.forEach {
            list.add(it)
        }
        return ResponseDTO.success(200, list, mutableListOf("Posts successfully listed!"))
    }

    override fun findById(id: UUID): ResponseDTO {
        val post: Post
        try {
            post = postRepository.findById(id).get()
        } catch (e: Exception) {
            logger.error { e }
            throw CustomException("Post Not Found Exception")
        }
        return ResponseDTO.success(200, mutableListOf(post.toDTO()), mutableListOf("Post successfully Loaded!"))
    }

    override fun findAll(): ResponseDTO {
        val posts = postRepository.findAllByOrderByCreatedDesc()
        val list: MutableList<Any> = ArrayList()
        posts.forEach { post ->
            val postDTO = post.toDTO()
            if (!post.commentList.isNullOrEmpty()) {
                post.commentList!!.forEach { comment ->
                    val dto = comment.toDTO()
                    dto.post = null
                    postDTO.commentList!!.add(dto)
                }
            }
            list.add(postDTO)
        }
        return ResponseDTO.success(200, list, mutableListOf("Posts successfully listed!"))
    }

    override fun delete(postId: UUID, userId: UUID): ResponseDTO {
        val userResult: Optional<User> = useRepository.findById(userId)
        if (!userResult.isPresent) return ResponseDTO.error(400, mutableListOf("User does not exists!"))

        val postResult: Optional<Post> = postRepository.findById(postId)
        if (!postResult.isPresent) return ResponseDTO.error(400, mutableListOf("Post does not exists!"))

        return if (userResult.get().id == postResult.get().user.id) {
            postRepository.deleteById(postId)

            ResponseDTO.success(200, null, mutableListOf("Post and its Comments successfully deleted!"))
        } else {
            ResponseDTO.error(400, mutableListOf("Post cannot be deleted as User is not the author!"))
        }
    }
}