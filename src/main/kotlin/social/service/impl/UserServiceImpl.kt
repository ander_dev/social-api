package social.service.impl

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCrypt
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import social.entity.User
import social.entity.dto.ResponseDTO
import social.entity.dto.UserDTO
import social.exception.CustomException
import social.repository.UserRepository
import social.service.UserService
import java.util.*

@Service("userService")
class UserServiceImpl : UserService {

    private val logger = KotlinLogging.logger {}

    @Autowired
    lateinit var userRepository: UserRepository

    private fun validateUser(userDTO: UserDTO) : MutableList<String>?{
        val errors: MutableList<String>? = ArrayList()
        if (userDTO.name.isNullOrEmpty()) {
            errors!!.add("Name must be informed!")
        }
        if (userDTO.email.isNullOrEmpty()) {
            errors!!.add("E-mail address must be informed!")
        }
        if (userDTO.password.isNullOrEmpty()) {
            errors!!.add("Password must be informed!")
        }
        return errors
    }

    @Transactional
    override fun save(userDTO: UserDTO): ResponseDTO {
        val user: User
        val message = if (userDTO.id == null)  "User successfully saved!" else "User successfully updated!"
        try {
            val errors = validateUser(userDTO)
            if (errors!!.isNotEmpty()) {
                return ResponseDTO.error(400, errors)
            }
            userDTO.password = BCryptPasswordEncoder().encode(userDTO.password)
            user = userRepository.save(User.fromDTO(userDTO))
        } catch (e: Exception) {
            logger.error { e }
            throw CustomException(e.message!!)
        }
        return ResponseDTO.success(200, mutableListOf(user.toDTO()), mutableListOf(message))
    }

    override fun findById(id: UUID): ResponseDTO {
        val user: User
        try {
            user = userRepository.findById(id).get()
        } catch (e: Exception) {
            logger.error { e }
            throw CustomException("User Not Found Exception")
        }
        return ResponseDTO.success(200, mutableListOf(user.toDTO()), mutableListOf("User successfully Loaded!"))
    }

    override fun delete(id: UUID): ResponseDTO {
        val user = userRepository.findById(id).get()
        user.excluded = true
        userRepository.save(user)
        return ResponseDTO.success(200, mutableListOf(user.toDTO()), mutableListOf("User successfully deleted!"))
    }

    override fun login(email: String, password: String): ResponseDTO? {
        var user = userRepository.findByEmail(email)
        if (user.isPresent) {
            if (BCrypt.checkpw(password, user.get().password)) {
                return ResponseDTO.success(200, mutableListOf(user.get()), mutableListOf("${user.get().name} successfully logged in!"))
            }
        }
        return ResponseDTO.error(400, mutableListOf("Please verify your credentials!"))
    }

    override fun deleteUserAutomation(email: String) {
        val user: Optional<User> = userRepository.findByEmail(email)

        if (user.isPresent) {
            userRepository.deleteById(user.get().id!!)
        }
    }
}