package social.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties
class PropertiesValues {

    @Value("\${spring.security.user.name}")
    lateinit var securityUserName: String

    @Value("\${spring.security.user.password}")
    lateinit var securityUserPassword: String

    @Value("\${server.uri}")
    lateinit var apiURI: String

    @Value("\${server.port}")
    lateinit var apiPort: String

    @Value("\${server.protocol}")
    lateinit var apiProtocol: String

    @Bean("securityUser")
    fun getSecurityUsername(): String {
        return securityUserName
    }

    @Bean("securityPassword")
    fun getSecurityUserpassword(): String {
        return securityUserPassword
    }

    @Bean("apiURL")
    fun getApiUrl(): String {
        return "$apiProtocol$apiURI:$apiPort"
    }
}