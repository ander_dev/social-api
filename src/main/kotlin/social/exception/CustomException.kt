package social.exception

class CustomException(message: String) : RuntimeException(message)