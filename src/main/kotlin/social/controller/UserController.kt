package social.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import social.entity.dto.ResponseDTO
import social.entity.dto.UserDTO
import social.service.UserService
import java.util.*
import javax.ws.rs.FormParam
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/api/user"], produces = [MediaType.APPLICATION_JSON])
class UserController {

    @Autowired
    lateinit var userService: UserService

    @PostMapping
    fun save (@RequestBody userDTO: UserDTO): ResponseDTO = userService.save(userDTO)

    @GetMapping("/{id}")
    fun getById(@PathVariable("id") id: UUID): ResponseDTO = userService.findById(id)

    @RequestMapping("/{id}", method = [(RequestMethod.DELETE)])
    fun delete (@PathVariable("id") id: UUID): ResponseDTO = userService.delete(id)

    @RequestMapping("/login", method = [(RequestMethod.POST)])
    fun login(@FormParam("email") email: String, @FormParam("password") password: String): ResponseDTO? {
        return userService.login(email, password)
    }
}