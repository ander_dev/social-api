package social.controller

import mu.KotlinLogging
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import social.entity.dto.ResponseDTO
import javax.ws.rs.core.MediaType

@RestController
@RequestMapping(path = ["/api"], produces = [MediaType.APPLICATION_JSON])
class ApiController {

    private val logger = KotlinLogging.logger {}

    @GetMapping
    fun getStatus(): ResponseDTO {
        val response = ResponseDTO.success(200, null, mutableListOf("Application up and Running!"))
        logger.info(response.messages.toString())
        return response
    }
}