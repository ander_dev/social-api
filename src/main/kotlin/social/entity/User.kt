package social.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import social.entity.dto.UserDTO
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "user",
        uniqueConstraints = [UniqueConstraint(name = "unique_user_email", columnNames = arrayOf("email"))],
        indexes = [
            Index(name = "idx_user_id",  columnList="id", unique = true),
            Index(name = "idx_user_email",  columnList="email", unique = true)
        ]
)
data class User(
        @Id
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "uuid2")
        @Type(type="uuid-char")
        var id: UUID? = null,
        var name: String,
        var email: String,
        var password: String,
        @Column(columnDefinition="bit default 0")
        var excluded: Boolean = false,
        var registered: LocalDateTime? = null,
        @OneToMany(mappedBy = "user", cascade = [CascadeType.REMOVE], fetch = FetchType.EAGER)
        @JsonIgnore
        var postList: List<Post?>? = null
) {
    override fun toString(): String {
        return "User(id=$id, name='$name', email='$email', password='$password', excluded=$excluded, registered=$registered)"
    }

    fun toDTO(): UserDTO = UserDTO(
            id = this.id!!,
            name = this.name,
            email = this.email,
            password = this.password,
            excluded = this.excluded
    )

    companion object {

        fun fromDTO(dto: UserDTO) = User(
                id = dto.id,
                name = dto.name!!,
                email = dto.email!!,
                password = dto.password!!,
                excluded =  dto.excluded,
                registered = LocalDateTime.now())
    }
}
