package social.entity

import org.hibernate.annotations.GenericGenerator
import org.hibernate.annotations.Type
import social.entity.dto.CommentDTO
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "comment",
        indexes = [Index(name = "idx_comment_id", columnList = "id", unique = true)]
)
data class Comment(
        @Id
        @GeneratedValue(generator = "uuid2")
        @GenericGenerator(name = "uuid2", strategy = "uuid2")
        @Type(type = "uuid-char")
        var id: UUID? = null,
        var message: String,
        @ManyToOne(cascade = [CascadeType.REFRESH], targetEntity = Post::class, fetch = FetchType.EAGER)
        @JoinColumn(name = "post_id", foreignKey = ForeignKey(name = "FK_COMMENT_POST"))
        var post: Post,
        @ManyToOne(cascade = [CascadeType.REFRESH], targetEntity = User::class, fetch = FetchType.EAGER)
        @JoinColumn(name = "user_id", foreignKey = ForeignKey(name = "FK_COMMENT_USER"))
        var user: User,
        var created: LocalDateTime? = null
) {

    override fun toString(): String {
        return "Comment(id=$id, message='$message', post=$post, user=$user, created=$created)"
    }

    fun toDTO(): CommentDTO = CommentDTO(
            id = this.id!!,
            message = this.message,
            post = this.post.toDTO(),
            user = this.user.toDTO(),
            created = this.created
    )

    companion object {

        fun fromDTO(dto: CommentDTO) = Comment(
                id = dto.id,
                message = dto.message!!,
                post = Post.fromDTO(dto.post!!),
                user = User.fromDTO(dto.user!!),
                created = LocalDateTime.now())
    }
}
