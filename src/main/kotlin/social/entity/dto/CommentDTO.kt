package social.entity.dto

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime
import java.util.*

data class CommentDTO(
        var id: UUID? = null,
        var message: String? = null,
        var post: PostDTO? = null,
        var user: UserDTO? = null,
        @get:JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        var created: LocalDateTime? = null
) {
    override fun toString(): String {
        return "CommentDTO(id=$id, message=$message, created=$created, post=$post, user=$user)"
    }
}
