package social.entity.dto

import java.util.*

data class UserDTO(
        var id: UUID? = null,
        var name: String? = null,
        var email: String? = null,
        var password: String? = null,
        var excluded: Boolean = false
) {
    override fun toString(): String {
        return "UserDTO(id=$id, name=$name, email=$email, password=$password, excluded=$excluded)"
    }
}