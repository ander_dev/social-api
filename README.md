## Social API
### Is an API written in Kotlin designed to demnstrate my knowledge

Social API is using:

* Trello to organise and manage project development (https://trello.com/b/lDSTblFv/westy-board)
* Kotlin
* Kotlin-jvm
* Spring boot 2.3.1
* Spring security for basic auth as a fast security solution
* Spring JPA
* Cucumber + gherkins for tests
* MariaDB 
* logback with traceID and spamADI
* docker and docker-compose

### Requirements:
* Docker
* gradle 6 - only needed to run tests
* download the code from repo
* please note that .env file is pushed to the repo for demonstrate how to use, this file should not be pushed as it contains security credentials.

## How to run:
### to run Tests locally:
* Via IDE is the easiest way, create a runner from RunTest.kt located at src/test/kotlin and
* copy content from .env file in the root folder and add as env variables for the runner, in intelliJ it can be pasted all at once  
OR  
* to run via terminal on MAC, please follow steps below  
1. open terminal and execute command below, it will install ENV needed for the build  
````
export SOCIAL_API_MYSQL_ROOT_PASSWORD=rootpass &&
export SOCIAL_API_MYSQL_DATABASE=social &&
export SOCIAL_API_MYSQL_USER=social_user &&
export SOCIAL_API_MYSQL_PASSWORD=social_pass &&
export SOCIAL_API_MYSQL_HOST=localhost &&
export SOCIAL_API_MYSQL_PORT=3306 &&
export SOCIAL_API_HOST_PROTOCOL=http:// &&
export SOCIAL_API_HOST_URL=localhost &&
export SOCIAL_API_HOST_PORT=8086 &&
export SOCIAL_API_BASIC_USER=social_api_user &&
export SOCIAL_API_BASIC_PASSWORD=social_api_pass  &&
export SOCIAL_API_LOG_PATH=./build/logs &&
export SOCIAL_API_LOG_LEVEL=INFO 
````  
2. at the terminal run line below  
 
 * it will access db folder 
 * compose db for the tests 
 * run the tests 
 * stop compose
 * remove db container and image

````
cd db && docker-compose up -d && cd .. && gradle clean test --info && docker-compose down -v && docker rm social-db -f && docker rmi db_social-db -f
````

### to run API locally:
1. if ran tests via terminal, please close terminal and open another one, it will eliminate the local ENVs created on step 1
2. let's compose the API  

* at project folder run: 
* to start: ``docker-compose up -d``
* do finish: ``docker-compose down -v``

## to Access the API
* after docker composed
* access http://localhost:8086/swagger-ui.html for the API definition
* API credentials are: social_api_user and social_api_pass
* please note that credentials as well as .env file should not be specified here nor in the repo, for security purpose, just left it here for easiest way to demonstrate my solution.



